import React, {Component} from 'react';
import contacto from '../imgs/contacto/contacto.jpg';
import contacto2 from '../imgs/contacto/contacto2.jpg';
import '../css/Contact.css';

import ScrollReveal from 'scrollreveal';

export default class ContactComponent extends Component{

    componentDidMount(){
      const scrollRevealOption = {
          delay: 50,
          distance: '20%',
          duration: 1000,
          interval: 250
      }
      ScrollReveal().reveal('.slide-up', {
          ...scrollRevealOption,
          origin: 'bottom'
      })
    }

    render(){
        return (
            <div className= "pastel-contacto">
                <div className="slide-up"><img src= {contacto} alt= {"foto de creacion"}/></div>
                <div className="pastel-contacto-email">
                    <div className=" slide-up">
                        <h1>CONTACTO</h1>
                        <p>¿Quieres contactarnos? ¡Estaremos encantados de atenderte!
                        Puedes pasarte por uno de nuestros locales o  nosotros te responderemos lo más rápido posible luego de enviarnos tus dudas. Puedes llamarnos por teléfono, escribirnos un email o saludarnos por las Redes Sociales. Si tardamos en contestar, posiblemente estemos horneando nuevas recetas o creando nuestras próximas ideas para pasteles ingeniosos. Pero no te preocupes, siempre respondemos.<br/><br/>
                        +3465789345 <br/>
                        info@centralcake.com</p>
                    </div>
                   
                    <div className=" slide-up">
                        <img src= {contacto2} alt= {"foto de creacion"}/>
                    </div>
                </div>
            </div>
        )
    }
}
import React, {Component} from 'react';
import bannerImg from '../imgs/home.jpg';
import '../css/Home.css';
import creandoImg from '../imgs/creando.jpg';
import png1 from '../imgs/cupCakesInclinados/png1.png';
import png2 from '../imgs/cupCakesInclinados/png2.png';
import png3 from '../imgs/cupCakesInclinados/png3.png';
import png4 from '../imgs/cupCakesInclinados/png4.png';
import png5 from '../imgs/cupCakesInclinados/png5.png';
import png6 from '../imgs/cupCakesInclinados/png6.png';
import png7 from '../imgs/cupCakesInclinados/png7.png';
import png8 from '../imgs/cupCakesInclinados/png8.png';
import png9 from '../imgs/cupCakesInclinados/png9.png';
import pastel1 from '../imgs/tartas/pastel1.jpg';
import pastel2 from '../imgs/tartas/pastel2.jpg';
import pastel3 from '../imgs/tartas/pastel3.jpg';
import pastel4 from '../imgs/tartas/pastel4.jpg';
import pastel5 from '../imgs/tartas/pastel5.jpg';
import pastel6 from '../imgs/tartas/pastel6.jpg';
import pastel7 from '../imgs/tartas/pastel7.jpg';
import pastel8 from '../imgs/tartas/pastel8.jpg';
import pastel9 from '../imgs/tartas/pastel9.jpg';
import pastel10 from '../imgs/tartas/pastel10.jpg';
import pastel11 from '../imgs/tartas/pastel11.jpg';
import creandoImg2 from '../imgs/creando2.jpg';
import promocion from '../imgs/promocion.jpg';

import Slider from "react-slick";

import ScrollReveal from 'scrollreveal';

export default class HomeComponent extends Component{

    componentDidMount(){
      const scrollRevealOption = {
          delay: 50,
          distance: '20%',
          duration: 1000,
          interval: 250
      }
      ScrollReveal().reveal('.fade-in', {
          ...scrollRevealOption,
          distance: '0%'
      })
      ScrollReveal().reveal('.slide-up', {
          ...scrollRevealOption,
          origin: 'bottom'
      })
      ScrollReveal().reveal('.slide-left', {
          ...scrollRevealOption,
          origin: 'right'
      })
      ScrollReveal().reveal('.slide-down', {
          ...scrollRevealOption,
          origin: 'up',
          rotate: { z:-40 }
      })
    }

    render(){
        const settings = {
            infinite: true,
            speed: 500,
            slidesToShow: 4,
            centerMode: true,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 2000,
            variableWidth: true,
            className: "pastel-banner3-container slide-left"
        };
        return(
            <div>
                <div>
                    <img className="pastel-banner1 slide-up" src={bannerImg} alt={"banner"}/>
                </div>
                <div className="pastel-banner3-title slide-left">
                    <span>Nuestras Tartas</span>
                </div>
                <div className="pastel-banner3-description slide-left">
                    <span>Aqui puedes ver una seleccion de nuestras tartas mas pedidas, no dejes de llevartelas para tus celebraciones especiales.</span>
                </div>
                <Slider {...settings}>
                    <div><img className="hvr-grow" src={pastel1} alt={"pastel banner"} /></div>
                    <div><img className="hvr-grow" src={pastel2} alt={"pastel banner"} /></div>
                    <div><img className="hvr-grow" src={pastel3} alt={"pastel banner"} /></div>
                    <div><img className="hvr-grow" src={pastel4} alt={"pastel banner"} /></div>
                    <div><img className="hvr-grow" src={pastel5
                    } alt={"pastel banner"} /></div>
                    <div><img className="hvr-grow" src={pastel6} alt={"pastel banner"} /></div>
                    <div><img className="hvr-grow" src={pastel7} alt={"pastel banner"} /></div>
                    <div><img className="hvr-grow" src={pastel8} alt={"pastel banner"} /></div>
                    <div><img className="hvr-grow" src={pastel9} alt={"pastel banner"} /></div>
                    <div><img className="hvr-grow" src={pastel10} alt={"pastel banner"} /></div>
                    <div><img className="hvr-grow" src={pastel11} alt={"pastel banner"} /></div>
                </Slider>
                <div className="pastel-banner2-container slide-up">
                    <img className="pastel-banner2" src={creandoImg} alt={"banner"}/>
                    <div className="pastel-banner2-content fade-in">
                        <h2>ESPONJOSOS Y RECIÉN SALIDOS DEL HORNO, UNA DELICIA AL PALADAR</h2>
                        <p>Elaborados con los mejores ingredientes estos deliciosos y sabrosos pasteles en nuestro horno. De esta manera, nuestros clientes pueden disfrutar de un producto original que hace las delicias de mayores y pequeños.</p>
                    </div>
                </div>
                <div className="pastel-banner4-container">
                    <img className="slide-down hvr-buzz-out" src={png1} alt={"cupcake"}/>
                    <img className="slide-down hvr-buzz-out" src={png2} alt={"cupcake"}/>
                    <img className="slide-down hvr-buzz-out" src={png3} alt={"cupcake"}/>
                    <img className="slide-down hvr-buzz-out" src={png4} alt={"cupcake"}/>
                    <img className="slide-down hvr-buzz-out" src={png5} alt={"cupcake"}/>
                    <img className="slide-down hvr-buzz-out" src={png6} alt={"cupcake"}/>
                    <img className="slide-down hvr-buzz-out" src={png7} alt={"cupcake"}/>
                    <img className="slide-down hvr-buzz-out" src={png8} alt={"cupcake"}/>
                    <img className="slide-down hvr-buzz-out" src={png9} alt={"cupcake"}/>
                </div>
                <div className="pastel-banner2-container slide-up">
                    <img className="pastel-banner2" src={creandoImg2} alt={"banner"}/>
                    <div className="pastel-banner2-content fade-in">
                            <h2>DESCUBRE LOS PASTELES MÁS SUCULENTOS</h2>
                            <p>Cada día, los elaboramos con dedicación y mucho cariño. Porque sabemos que las cosas hechas con esmero siempre salen mejor. El resultado, un rico pastel que combina a la perfección con casi cualquier sabor. Acompañado de chocolate, Sirope de Arce, frutas e incluso con un sencillo glaseado, conquistan a quienes los prueban. Entre tanta variedad, ¡será difícil elegir sólo uno!</p>
                    </div>
                </div>
              
                <div className="pastel-banner5-container  slide-up">
                    <img className="fade-in" src={png9} alt={"cupcake"}/>
                    <div className="pastel-banner5-content fade-in">
                        <h2>HAZ TU DÍA MÁS ESPECIALES CON PASTELES </h2>
                        <p>Esponjosos y sabrosos, nuestros Pasteles son perfectos para acompañar cualquier ocasión y para disfrutarlos con quien más te apetezca. En familia, con los amigos, solo o en pareja; para celebrar un evento especial o simplemente porque sí… ¡te lo mereces!
                        Pero tenemos más, ¡mucho más! Una amplia variedad de propuestas que podrás disfrutar cuando quieras. Desayuna con los compañeros, merienda con los peques o disfruta del mejor picoteo con los amigos. Cualquier momento del día es perfecto para venir a la pastelería.
                        </p>
                    </div>
                </div>
                <div className="pastel-promocion-container slide-up">
                    <div><img  src={promocion} alt={"cupcake"}/></div>
                    <div className="fade-in">
                        <h2>PROMOCIÓN PARA TODA LA FAMILIA</h2>
                        <p>No esperes mas lleva tu pastel a casa y compartelo con toda la familia.Prueba los pasteles más deliciosos, nuestras tartas, disfruta del placer de comer nuestras magdalenas, comparte las mejores galletas… ¡todo está riquísimo! Acompáñalo, además, del mejor café o de uno de nuestros tés. Y, si lo tuyo es cuidarte, aprovecha la oportunidad de probar nuestros postres veganos. Entre tanta variedad te va a costar elegir, pero tenemos la solución… vuelve siempre que quieras. ¡Te esperamos con una sonrisa y muy “bon appetit”!
                        </p>
                    </div>
                </div>
            </div>
        )
    }
}
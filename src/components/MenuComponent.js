import React, {Component} from 'react'
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import '../css/Menu.css'
import logo from '../imgs/menuLogo.png'


export default class MenuComponent extends Component{
    render(){
        return (
            <ul className="pastel-menu">
                <li><Link to="/cake/">Maestros del Pastel</Link></li>
                <li><Link to="/store/">Tienda</Link></li>
                <li><Link to="/"><img className="pastel-logo hvr-bounce-in" src={logo} alt={"central cake"}/></Link></li>
                <li><Link to="/catering/">Catering&Eventos</Link></li>
                <li><Link to="/contact/">Contacto</Link></li>
            </ul>
        )
    }
}
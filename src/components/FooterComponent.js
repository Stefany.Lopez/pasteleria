import React,  {Component} from 'react';
import '../css/Footer.css';
import facebook from '../imgs/redesSociales/facebook.png';
import twitter from '../imgs/redesSociales/twitter.png';
import instagram from '../imgs/redesSociales/instagram.png';

export default class FooterComponent extends Component{
    render() {
        return (
            <div className="pastel-footer">
                <footer>
                    <p>© 2019 Central Cake All rights reserved.</p>
                    <p>Contact information: <a>Cakereserve@central.com</a>.</p>
                </footer>
                <div className="pastel-footer-social">
                    <a href="https://www.facebook.com"><img src={facebook} alt={"redes sociales"}/></a>
                    <a href="https://www.twitter.com"><img src={twitter} alt={"redes sociales"}/></a>
                    <a href="https://www.instagram.com"><img src={instagram} alt={"redes sociales"}/></a>
                </div>
            </div>
        )
    }
}
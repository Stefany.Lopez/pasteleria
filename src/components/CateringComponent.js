import React, {Component} from 'react';
import '../css/Catering.css';
import evento1 from '../imgs/eventosYCatering/evento1.jpg';
import evento2 from '../imgs/eventosYCatering/evento2.jpg';
import evento3 from '../imgs/eventosYCatering/evento3.jpg';
import evento4 from '../imgs/eventosYCatering/evento4.jpg';
import evento5 from '../imgs/eventosYCatering/evento5.jpg';
import evento6 from '../imgs/eventosYCatering/evento6.jpg';
import evento7 from '../imgs/eventosYCatering/evento7.jpg';
import evento8 from '../imgs/eventosYCatering/evento8.jpg';
import evento9 from '../imgs/eventosYCatering/evento9.jpg';
import evento10 from '../imgs/eventosYCatering/evento10.jpg';
import evento11 from '../imgs/eventosYCatering/evento11.jpg';
import evento12 from '../imgs/eventosYCatering/evento12.jpg';
import evento13 from '../imgs/eventosYCatering/evento13.jpg';
import evento14 from '../imgs/eventosYCatering/evento14.jpg';
import evento15 from '../imgs/eventosYCatering/evento15.jpg';
import evento16 from '../imgs/eventosYCatering/evento16.jpg';

import Slider from "react-slick";

import ScrollReveal from 'scrollreveal';

export default class CateringComponent extends Component{

    componentDidMount(){
      const scrollRevealOption = {
          delay: 50,
          distance: '20%',
          duration: 1000,
          interval: 250
      }
      ScrollReveal().reveal('.fade-in', {
          ...scrollRevealOption,
          distance: '0%'
      })
      ScrollReveal().reveal('.slide-up', {
          ...scrollRevealOption,
          origin: 'bottom'
      })
    }

    render() {
        const settings = {
            infinite: true,
            speed: 500,
            slidesToShow: 3,
            centerMode: true,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 2000,
            className: "pastel-eventos-container3 slide-up"
        };
        return (
            <div className="pastel-eventos-content slide-up" alt="eventos">
                <div className="pastel-eventos-container" alt="eventos">
                    <div>
                        <h1>CATERING & EVENTOS</h1>
                        <p>Pastelería abrió por primera vez sus puertas en Venezuela en el año 1999, creando un sabor inigualable en sus pasteles. Nuestras confecciones en el pastel nos han llevado más allá de la repostería, creamos tu evento especial para tus fiestas y celebraciones en toda ocasión, hacemos de tu evento algo inigualable!
                        
                        Donde sea tu nueva reunión en tu proximo dia, deja que pastelería te llene de emociones grandiosas en tu próximo evento.</p>
                    </div>
                    <img className="fade-in" src= {evento1} alt= {"foto del evento"}/>
                </div>
                <div className="pastel-eventos-container2" alt="eventos">
                    <div className=" slide-up">
                        <img src= {evento2} alt= {"foto del evento"}/>
                        <h2>EVENTOS CORPORATIVOS</h2>
                        <p>La mejor reunion del dia es con un trocito de pastel! The best meeting of the day is one with cupcakes! Recompensa e inspira a tus clientes y asociados con pastelería.</p>
                    </div>
                    <div className=" slide-up">
                        <img src= {evento3} alt= {"foto del evento"}/>
                        <h2>OCASIONES SOCIALES</h2>
                        <p>Pasteleria...La manera más deliciosa de celebrar! Ricos pasteles frescos un trato especial para todos tus dulces momentos.</p>
                    </div>
                    <div className=" slide-up">
                        <img src= {evento4} alt= {"foto del evento"}/>
                        <h2>CUSTOMIZA TU ESPACIO, CREA EL TUYO.</h2>
                        <p>Te creamos pequeños espacios elegidos por ti, cada detalle cada sabor.
                        Elige de una variedad de opciones y diseña el tuyo propio.</p>
                        </div>
                    <div className=" slide-up">
                        <img src= {evento5} alt= {"foto del evento"}/>
                        <h2>TIENDA DEL PASTEL</h2>
                        <p>Puedes comprar magdalenas con los sabores de tus pasteles favoritos.</p>
                    </div>
                   

                </div>
                <Slider {...settings}>
                
                    <img src= {evento6} alt= {"foto del evento"}/>
                    <img src= {evento7} alt= {"foto del evento"}/>
                    <img src= {evento8} alt= {"foto del evento"}/>
                    <img src= {evento9} alt= {"foto del evento"}/>
                    <img src= {evento10} alt= {"foto del evento"}/>
                    <img src= {evento11} alt= {"foto del evento"}/>
                    <img src= {evento12} alt= {"foto del evento"}/>
                    <img src= {evento13} alt= {"foto del evento"}/>
                    <img src= {evento14} alt= {"foto del evento"}/>
                    <img src= {evento15} alt= {"foto del evento"}/>
                    <img src= {evento16} alt= {"foto del evento"}/>
               
                </Slider>
            </div>
        )
    }
}
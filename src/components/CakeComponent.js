import React, {Component} from 'react';
import creando3 from '../imgs/maestrosDelPastel/creando3.jpg';
import creando4 from '../imgs/maestrosDelPastel/creando4.jpg';
import cupcake from '../imgs/cupcake.jpg';
import galletas from '../imgs/galletas.jpg';
import '../css/Cake.css';

import ScrollReveal from 'scrollreveal';


export default class CakeComponent extends Component{

    componentDidMount(){
      const scrollRevealOption = {
          delay: 50,
          distance: '20%',
          duration: 1000,
          interval: 250
      }
      ScrollReveal().reveal('.slide-up', {
          ...scrollRevealOption,
          origin: 'bottom'
      })
    }

    render(){
        return (
            <div>
                <div className="pastel-maestros-container slide-up">
                    <div>
                        <img src={creando4} alt={"imagen de tienda"}/>
                    </div>
                </div>
                <div className="pastel-maestros2-container slide-up">
                    <h1>
                        Maestros del<br/>
                         pastel
                    </h1>
                    <p>
                        Su aroma te invita a viajar al pasado. Huele a la calidez del hogar y a recuerdos de la infancia nada como un buen pastel, una galleta con la receta de la abuela. Cuando nuestras abuelas hacían pasteles de vainilla, nos preparaban leche bien fría con un trocito de pastel para refrescar los calurosos días de verano o endulzaban aún más la Navidad enseñándonos a preparar galletas y magdalenas.<br/>
                        Llevamos años trabajando y estudiando sus matices, con el fin de crear y perfeccionar una receta muy nuestra traída de muy lejos: los pasteles. Hoy, podemos decir con orgullo que no sólo tenemos una receta propia, sino que además hemos conseguido elaborar los mejores Pasteles .<br/>
                         Gracias a esta dedicación y al cariño que le hemos puesto nos hemos convertido en todos unos Maestros del Pastel.<br/>
                        Desde el año 1999 elaboramos artesanalmente y con los mejores ingredientes, entre los que se encuentra el auténtico horneado perfecto , estos esponjosos y sabrosos dulces.
                    </p>
                </div>
                <div className="pastel-maestros3-container slide-up">
                        <img src={creando3} alt={"imagen de tienda"}/>
                    <div className="pastel-maestros3-container2">
                        <img src={cupcake} alt={"imagen de tienda"}/>
                        <img src={galletas} alt={"imagen de tienda"}/> 
                    </div>    
                </div>
            </div>
        )
    }

}
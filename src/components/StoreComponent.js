import React, {Component} from 'react';
import '../css/Store.css';
import pastelprincipal from '../imgs/tienda_banner.jpg';
import pastel1 from '../imgs/tartas/pastel1.jpg';
import pastel2 from '../imgs/tartas/pastel2.jpg';
import pastel3 from '../imgs/tartas/pastel3.jpg';
import pastel4 from '../imgs/tartas/pastel4.jpg';
import pastel5 from '../imgs/tartas/pastel5.jpg';
import pastel6 from '../imgs/tartas/pastel6.jpg';
import pastel7 from '../imgs/tartas/pastel7.jpg';
import pastel8 from '../imgs/tartas/pastel8.jpg';
import pastel9 from '../imgs/tartas/pastel9.jpg';
import pastel10 from '../imgs/tartas/pastel10.jpg';
import pastel11 from '../imgs/tartas/pastel11.jpg';
import pastel12 from '../imgs/tartas/pastel1.jpg';
import pastel13 from '../imgs/tartas/pastel2.jpg';
import pastel14 from '../imgs/tartas/pastel3.jpg';
import pastel15 from '../imgs/tartas/pastel4.jpg';
import pastel16 from '../imgs/tartas/pastel16.jpg';

import ScrollReveal from 'scrollreveal';


export default class StoreComponent extends Component{

    componentDidMount(){
      const scrollRevealOption = {
          delay: 50,
          distance: '20%',
          duration: 1000,
          interval: 250
      }
      ScrollReveal().reveal('.slide-up', {
          ...scrollRevealOption,
          origin: 'bottom'
      })
      ScrollReveal().reveal('.slide-down', {
          ...scrollRevealOption,
          origin: 'up'
      })
    }

    render(){
        return(
            <div className="pastel-tienda">

                <div className="pastel-principal slide-up">
                     <img src={pastelprincipal} alt={"pastel banner"} />
                </div>

                <div className="pastel-tienda-titulo slide-up">
                    <h1>Nuestras tartas</h1>
                </div>

                <div className="pastel-pasteles">
                    <div className="pastel-pastel slide-down">
                        <div>
                             <img className="hvr-grow" src={pastel1} alt={"pastel banner"} />
                        </div> 
                        <div className="pastel-pastel-content">
                            <h1>Pastel de Vainilla relleno de limón</h1>
                            Para 50 personas<br/>
                            Tres pisos<br/>
                            Decoración en mazapán<br/>
                            Flores comestibles<br/>
                            Base de madera a elegir<br/>
                        </div>
                    </div>

                    <div className="pastel-pastel  slide-down">
                        <div>
                             <img className="hvr-grow" src={pastel2} alt={"pastel banner"} />
                        </div>
                        <div className="pastel-pastel-content">
                        <h1>Pastel de Vainilla relleno Naranja</h1>
                            Para 20 personas<br/>
                            Tres pisos<br/>
                            Decoración  en crema de mantequilla<br/>
                            Flores comestibles y macarrones<br/>
                            Base de madera a elegir<br/>
                        </div>
                    </div>

                    <div className="pastel-pastel  slide-down">
                        <div>
                             <img className="hvr-grow" src={pastel3} alt={"pastel banner"} />
                        </div>
                        <div className="pastel-pastel-content">
                        <h1>Pastel de chocolate relleno de crema de avellanas</h1>
                            Para 30 personas<br/>
                            Cuatro pisos<br/>
                            Decoraciónn en mazapán<br/>
                            Mariposas de colores pasteles<br/>
                            Base de madera a elegir<br/>
                        </div>
                    </div>

                    <div className="pastel-pastel  slide-down">
                        <div>
                             <img className="hvr-grow" src={pastel4} alt={"pastel banner"} />
                        </div>
                        <div className="pastel-pastel-content">
                        <h1>Pastel de Vainilla relleno de frambuesas</h1>
                            Para 15 personas<br/>
                            Dos mini pisos<br/>
                            Decoración en crema<br/>
                            Flores naturales con topper love<br/>
                            Base de madera a elegir<br/>
                        </div>
                    </div>
                    
                    <div className="pastel-pastel  slide-down">    
                        <div>
                             <img className="hvr-grow" src={pastel5} alt={"pastel banner"} />
                        </div>
                        <div className="pastel-pastel-content">   
                        <h1>Pastel de limón relleno de fresas</h1>
                            Para 70 personas<br/>
                            Cinco pisos<br/>
                            Decoración en mazapán<br/>
                            Flores comestibles y naturales<br/>
                            Base de madera a elegir<br/>  
                        </div>   
                    </div> 

                    <div className="pastel-pastel  slide-down"> 
                        <div>
                             <img className="hvr-grow" src={pastel6} alt={"pastel banner"} />
                        </div>
                        <div className="pastel-pastel-content"> 
                        <h1>Pastel de Vainilla relleno de Almendras</h1>
                            Para 50 personas<br/>
                            Tres pisos<br/>
                            Decoración en mazapán<br/>
                            Flores comestibles en mazapán<br/>
                            Base de madera a elegir<br/>  
                        </div>
                    </div> 

                    <div className="pastel-pastel  slide-down">
                        <div>
                             <img className="hvr-grow" src={pastel7} alt={"pastel banner"} />
                        </div>
                        <div className="pastel-pastel-content">
                        <h1>Pastel de Chocolate relleno de chocolate negro</h1>
                            Para 40 personas<br/>
                            Dos pisos<br/>
                            Decoración en crema<br/>
                            Flores naturales<br/>
                            Base de madera a elegir<br/>
                        </div>
                    </div>

                    <div className="pastel-pastel  slide-down">
                        <div>
                             <img className="hvr-grow" src={pastel8} alt={"pastel banner"} />
                        </div>
                        <div className="pastel-pastel-content">
                        <h1>Pastel de Vainilla relleno de chocolate blanco</h1>
                            Para 10 personas<br/>
                            1 piso<br/>
                            Decoración en crema rosa<br/>
                            chispas de serpentina<br/>
                            Base de madera a elegir<br/>
                        </div>
                    </div>

                    <div className="pastel-pastel  slide-down">
                        <div>
                             <img className="hvr-grow" src={pastel9} alt={"pastel banner"} />
                        </div>
                        <div className="pastel-pastel-content">
                        <h1>Pastel de Vainilla relleno de limón</h1>
                        Para 50 personas<br/>
                        Tres pisos<br/>
                        Decoración en mazapán<br/>
                        Flores comestibles<br/>
                        Base de madera a elegir<br/>
                        </div>
                    </div>

                    <div className="pastel-pastel  slide-down">
                        <div>
                             <img className="hvr-grow" src={pastel10} alt={"pastel banner"} />
                        </div>
                        <div className="pastel-pastel-content">
                        <h1>Pastel de Vainilla relleno de frambuesas y cerezas</h1>
                        Para 20 personas<br/>
                        Tres mini pisos<br/>
                        Decoración en mazapán<br/>
                        Flores comestibles<br/>
                        Base de madera a elegir<br/>
                        </div>
                    </div>

                    <div className="pastel-pastel  slide-down">
                        <div>
                             <img className="hvr-grow" src={pastel11} alt={"pastel banner"} />
                        </div>
                        <div className="pastel-pastel-content">
                        <h1>Pastel de Vainilla relleno de Naranja</h1>
                            Para 40 personas<br/>
                            Tres pisos<br/>
                            Decoración en mazapán<br/>
                            Flores comestibles<br/>
                            Base de madera a elegir<br/>
                        </div>
                    </div>

                    <div className="pastel-pastel  slide-down">
                        <div>
                             <img className="hvr-grow" src={pastel12} alt={"pastel banner"} />
                        </div>
                        <div className="pastel-pastel-content">
                        <h1>Pastel de Vainilla relleno de coco</h1>
                            Para 70 personas<br/>
                            Cuatro pisos<br/>
                            Decoración en mazapán<br/>
                            Flores comestibles y naturales<br/>
                            Base de madera a elegir<br/>
                        </div>
                    </div>

                    <div className="pastel-pastel  slide-down">
                        <div>
                             <img className="hvr-grow" src={pastel13} alt={"pastel banner"} />
                        </div>
                        <div className="pastel-pastel-content">
                        <h1>Pastel de chocolate blanco relleno de limón</h1>
                            Para 40 personas<br/>
                            Tres pisos<br/>
                            Decoración en mazapán<br/>
                            Flores comestibles<br/>
                            Base de madera a elegir<br/>
                        </div>
                    </div>

                    <div className="pastel-pastel  slide-down">
                        <div>
                             <img className="hvr-grow" src={pastel14} alt={"pastel banner"} />
                        </div>
                        <div className="pastel-pastel-content">
                        <h1>Pastel de Vainilla relleno de banana</h1>
                            Para 50 personas<br/>
                            Tres pisos<br/>
                            Decoración en mazapán<br/>
                            Flores comestibles<br/>
                            Base de madera a elegir<br/>
                        </div>
                    </div>

                    <div className="pastel-pastel  slide-down">
                        <div>
                             <img className="hvr-grow" src={pastel15} alt={"pastel banner"} />
                        </div>
                        <div className="pastel-pastel-content">
                        <h1>Pastel de Vainilla relleno de fresas</h1>
                            Para 10 personas<br/>
                            1 pisos<br/>
                            Decoración Flores<br/>
                            Flores naturales<br/>
                            Base de madera a elegir<br/>
                        </div>
                    </div>

                    <div className="pastel-pastel  slide-down">
                        <div>
                             <img className="hvr-grow" src={pastel16} alt={"pastel banner"} />
                        </div>
                        <div className="pastel-pastel-content">
                        <h1>Pastel de zanahoria relleno de crema mantequilla</h1>
                            Para 10 personas<br/>
                            Tres mini pisos<br/>
                            Decoración en mazapán<br/>
                            Flores naturales<br/>
                            Base de madera a elegir<br/>
                        </div>
                    </div>
                </div>

            </div>
        )
    }
}
import React, {Component} from 'react';
import './App.css';
import MenuComponent from './components/MenuComponent';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import HomeComponent from './components/HomeComponents';
import CakeComponent from './components/CakeComponent';
import StoreComponent from './components/StoreComponent';
import CateringComponent from './components/CateringComponent';
import ContactComponent from './components/ContactComponent';
import FooterComponent from './components/FooterComponent';

class App extends Component {

  render() {
    return (
      <Router>
        <div>
          <MenuComponent></MenuComponent>
          <Route path="/" exact component={HomeComponent} />
          <Route path="/cake/" component={CakeComponent} />
          <Route path="/store/" component={StoreComponent} />
          <Route path="/catering/" component={CateringComponent} />
          <Route path="/contact/" component={ContactComponent} />
          <FooterComponent></FooterComponent>
        </div>
      </Router>
    );
  }
}

export default App;
